const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: "development",
    entry: ["@babel/polyfill", "./src/index.js"],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: "babel-loader",
                enforce: "pre"
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
            },
            {
                test: /\.riot$/,
                exclude: /node_modules/,
                use: {
                    loader: '@riotjs/webpack-loader',
                    options: {
                        hot: true
                    }
                }
            },
            {
                test: /\.(png|jpg|jpeg|gif|mp4)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]", // "[hash].[ext]" for hashed file names
                            outputPath: "img/"
                        }
                    }
                ]
            },
            {
                test: /\.(eot|otf|ttf)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: "fonts/"
                        }
                    }
                ]
            }
        ]
    },
    node: {
        fs: "empty"
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            hash: true
        }),
        new Dotenv({
            path: path.resolve(__dirname, "./variables.env")
        }),
        new MiniCssExtractPlugin({
            // Option similar to the same options in webpackOptions.output
            // both options are options
            filename: "[name].css"
        }),
        new CopyWebpackPlugin(["src/pwa/manifest.json", "src/pwa/sw.js"], {
            ignore: [".DS_Store"]
        })
    ]
};
