<h1><img height="50px" src="">Arquitetura em Versos web site</h1>
<h2>Try in <a href="https://ismaelpamplona.gitlab.io/arquiteturaemversos/">https://ismaelpamplona.gitlab.io/arquiteturaemversos/</a></h2>

## Table of Contents

1. [Install](#install)
2. [Stack Used](#stack-used)
3. [Front-End Architecture](#front-end-architecture)

<h2 align="center">Install</h2>

```bash
npm install
```

Then run:

```bash
npm start
```

<h2 align="center">Stack Used</h2>

| Name                                                                                                                                                                                                                                                                                           | Link                                       | Description                                                                                                                                                                                                                                                    |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <a href="https://riot.js.org/"><img height="30px" src="https://riot.js.org/img/logo/riot240x.png"></a>                                                                                                                                                                                         | https://riot.js.org/                       | Component-based UI library                                                                                                                                                                                                                                     |
| <a href=""><img height="30px" src="https://redux.js.org/img/redux.svg"></a>                                                                                                                                                                                                                    | https://redux.js.org/                      | A predictable state container for JavaScript apps                                                                                                                                                                                                              |
| <a href="https://redux-saga.js.org/"><img height="30px" width="auto" src="https://camo.githubusercontent.com/4354872161943c4ae2ceec2a946dec85b96799b8/68747470733a2f2f72656475782d736167612e6a732e6f72672f6c6f676f2f303830302f52656475782d536167612d4c6f676f2d4c616e6473636170652e706e67"></a> | https://redux-saga.js.org/                 | Redux-saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, easy to test, and better at handling failures |
| <a href="https://sass-lang.com/"><img height="30px" src="https://sass-lang.com/assets/img/logos/logo-b6e1ef6e.svg"></a>                                                                                                                                                                        | https://sass-lang.com                      | CSS with superpowers                                                                                                                                                                                                                                           |
| <a href="https://webpack.js.org/"><img height="30px" src="https://webpack.js.org/e0b5805d423a4ec9473ee315250968b2.svg"></a>                                                                                                                                                                    | https://webpack.js.org/                    | A static module bundler for modern JavaScript applicationss                                                                                                                                                                                                    |
| <a href="https://github.com/mrsteele/dotenv-webpack">dotenv-webpack</a>                                                                                                                                                                                                                        | https://github.com/mrsteele/dotenv-webpack | A secure webpack plugin that supports dotenv and other environment variables and only exposes what you choose and use                                                                                                                                          |
| <a href="https://babeljs.io/"><img height="30px" src="https://d33wubrfki0l68.cloudfront.net/7a197cfe44548cc1a3f581152af70a3051e11671/78df8/img/babel.svg"></a>                                                                                                                                 | https://babeljs.io/                        | A JavaScript compiler                                                                                                                                                                                                                                          |
| <a href="https://github.com/axios/axios">Axios</a>                                                                                                                                                                                                                                             | https://github.com/axios/axios             | Promise based HTTP client for the browser and node.js                                                                                                                                                                                                          |

<h2 align="center">Front-End Architecture:</h2>

**Organizing Files and Folder Structure**

```
.
├── src/
│   ├── components/
│   │   ├── sub-component-1/
│   │   │   ├── sub-component-1.js
│   │   │   ├── sub-component-1.scss
│   │   │   └── sub-component-1.html
│   │   ├── sub-component-2/
│   │   │   └── (...)
│   │   └──sub-component-3/
│   │   │   └── (...)
│   │   ├── component.js
│   │   ├── component.scss
│   │   └── component.tag.html
│   ├── fonts/
│   ├── img/
│   ├── index.html
│   ├── index.js
├── configuration-file-1
├── configuration-file-2
├── configuration-file-3
├── configuration-file-4
├── configuration-file-5
├── configuration-file-6
└── (...)

```

**Explaning...**

<p>The idea is the component being totally app independent.</p>

-   `.scss`: Each component, if needs, will have it's own`.scss` and the configurations

```scss
.component-container {
    background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
        url("../../img/component-background-image.jpg");
    background-size: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
```

-   `.js`: Each component will have it's own `component-X.js` that will centralize everything mixins, scss, etc...

```js
import riot from "riot";
import "./component.scss";

import componentImage from "../../img/component-image-1.png";

riot.mixin("img-1-component", {
    componentImage
});
```

-   The folders 'img/' and 'fonts' will be necessary only if the these files (images/fonts) are specifically and component unique. In other words, only if other components don't need these folders or make no sense in these files root folders (general context).
