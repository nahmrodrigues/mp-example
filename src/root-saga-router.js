import store from "./store";
import route from "riot-route";
import catchErrors from "./handlers/error-handler";
import { takeEvery, call, all } from "redux-saga/effects";
import * as appController from "./app-controller";
import productDetailSaga from "./components/product-detail/product-detail-saga";

const executeRoute = (...params) => {
    const query = route.query();
    // If query is an empty object, return original params
    // If query exists, remove the last params item (that is the query)
    if (Object.keys(query).length !== 0) {
        params.pop();
    }

    return store.dispatch({
        type: "ROUTE",
        payload: {
            params,
            query
        }
    });
};

// start and configure riot-route
route.base("#/");
route(executeRoute); // for each route change, run executeRoute function above who will dispatch an action with the type 'ROUTE'
route.start(true);

function* handleRoute(action) {
    switch (action.payload.params[0]) {
        case "": {
            yield call(appController.home, action);
            break;
        }

        case "product-detail": {
            yield call(appController.productDetail, action);
            break;
        }

        case "loading": {
            yield call(appController.loading);
            break;
        }
    }
}

function* sagaRoute() {
    yield takeEvery("ROUTE", catchErrors, handleRoute); // for every dispatch with the type ROUTE will run the handleRoute function
    // yield takeEvery("ROUTE", handleRoute); // for every dispatch with the type ROUTE will run the handleRoute function
}

export default function* rootSaga() {
    yield all([
        call(sagaRoute),
        call(productDetailSaga)
    ]);
}
  
