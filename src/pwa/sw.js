// this file sets the SERVICE WORKER for a Progressive Web Application - PWA

let CACHE_STATIC_NAME = "static";
let CACHE_DYNAMIC_NAME = "dynamic";
const STATIC_FILES = [
    "/",
    "/index.html",
    "/bundle.js",
    "/main.css",
    "/img/landing.jpg",
    "/fonts/019fbcff45099c60890fd411396f6c3c.ttf",
    "/img/arquiteturaemversos-favicon.png",
    "/img/arquiteturaemversos-logo.png"
];

// https://developer.mozilla.org/en-US/docs/Web/API/Cache
self.addEventListener("install", function(e) {
    e.waitUntil(
        caches.open(CACHE_STATIC_NAME).then(function(cache) {
            cache.addAll(STATIC_FILES);
        })
    );
});

self.addEventListener("activate", function(e) {
    return self.clients.claim();
});

self.addEventListener("fetch", function(e) {
    e.respondWith(
        caches.match(e.request).then(function(res) {
            if (res) {
                return res;
            } else {
                return fetch(e.request);
            }
        })
    );
});
