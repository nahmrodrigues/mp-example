import * as api from "./handlers/api-handler";

import { put } from "redux-saga/effects";

export function* home(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_HOME_PAGE_ON"
    });
}

export function* productDetail(action) {
    yield put({
        type: "TURN_ALL_PAGES_OFF"
    });
    yield put({
        type: "TURN_PRODUCT_DETAIL_PAGE_ON",
        payload: action.payload.params[1]
    })
}

export function* submitCheckoutPayment(action) {
    yield api.submitCheckoutPayment(action.payload);
}

export function* loading(action) {
    yield put({
        type: "LOADING"
    });
}