import '@riotjs/hot-reload'
import { component, install } from 'riot'
import store from './store'
import App from './components/app.riot'

install(function(component) {
  component.store = { ...store }
})

component(App)(document.getElementById('view'))