import { takeLatest, call, all } from "redux-saga/effects";
import * as appController from "../../app-controller";

function* handleSubmitCheckoutPurchase(action) {
    yield call(appController.loading);
    yield call(appController.submitCheckoutPayment, action);
}

export default function* productDetailSaga() {
    yield all([
        takeLatest("SUBMIT_CHECKOUT_PURCHASE", handleSubmitCheckoutPurchase)
    ]);
}
