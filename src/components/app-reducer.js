import { products } from "./products";

const initialState = {
    view: {},
    alertMsgs: [],
    products: products
};

export default function appReducer(state = initialState, action) {
    switch (action.type) {
        case "TURN_ALL_PAGES_OFF": {
            const view = {};
            return { ...state, view };
        }

        case "TURN_HOME_PAGE_ON": {
            const view = {
                ...state.view,
                isHomePageVisible: true
            };
            return { ...state, view };
        }

        case "TURN_PRODUCT_DETAIL_PAGE_ON": {
            const view = {
                ...state.view,
                isProductDetailPageVisible: true
            };

            const productId = action.payload;
            const product = state.products[parseInt(productId)];
            
            return {...state, view, product}
        }

        case "LOADING": {
            const view = {
                ...state.view,
                isLoadingPageVisible: true
            };

            return { ...state, view };
        }

        case "CHECKOUT_REDIRECT": {
            const view = {
                ...state.view,
                isCheckoutRedirectActive: true
            };

            const redirectCheckoutUrl = action.payload;

            return {...state, view, redirectCheckoutUrl}
        }

        default: {
            return state;
        }
    }
}
