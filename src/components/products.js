import cellImg from "../img/cell.jpg";
import notebookImg from "../img/notebook.jpg";
import headphoneImg from "../img/headphone.jpg";

export const products = [
    {
        id : 0,
        title : "Celular",
        unit_price : 550.99,
        img: cellImg
    },
    {
        id : 1,
        title : "Notebook",
        unit_price : 1099.90,
        img: notebookImg
    },
    {
        id : 2,
        title : "Fone de Ouvido Sem Fio",
        unit_price: 199.00,
        img: headphoneImg
    }

];