import store from "../store";

export default function* catchErrors(fn, ...args) {
    try {
        yield* fn(...args);
    } catch (err) {
        let numbError = parseInt(
            err
                .toString()
                .match(/\d/g)
                .join("")
        );

        if (numbError === 502) {
            console.log("ERROR 502");
        } else if (numbError === 400) {
            console.log("ERROR 400");
        } else if (numbError === 500) {
            console.log("ERROR 500");
        } else if (numbError === 404) {
            console.log("ERROR 404");
            window.open("#/notfound", "_self");
        }
        // console.error("error-handler.js:", err.toString());
    }
}
