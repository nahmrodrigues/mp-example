import axios from "axios";
const apiUrl = "https://mp-example-api.herokuapp.com/";
// const apiUrl = "http://localhost:8888/";
import store from "../store";

export function* submitCheckoutPayment(data) {
    const url = apiUrl + "payment/mp-checkout";
    yield axios
        .post(url, data)
        .then(function(response) {
            store.dispatch({
                type: "CHECKOUT_REDIRECT",
                payload: response.data
            });
        })
        .catch(function(error) {
            console.log(error);
        });
}


// export function* getPosts(advance) {
//     const url = apiUrl + "aev/getpostlist";
//     const actualPage = Number(store.getState().appReducer.page);
//     const page = actualPage + advance;
//     const params = {
//         tagsQuery: store.getState().appReducer.tagsQuery,
//         dateQuery: store.getState().appReducer.dateQuery,
//         order: store.getState().appReducer.order,
//         page,
//         numel: store.getState().appReducer.numel
//     };
//     let filteredData = [];
//     yield axios
//         .post(url, params)
//         .then(function(response) {
//             response.data.result.posts.forEach(element => {
//                 let el = translation.postTranslation(element);
//                 filteredData.push(el);
//             });

//             store.dispatch({
//                 type: "UPDATE_POSTS",
//                 payload: {
//                     nextPage: response.data.result.nextPage,
//                     count: response.data.result.count,
//                     posts: filteredData,
//                     page: page
//                 }
//             });

//             if(store.getState().appReducer.tagsQuery) {
//                 store.dispatch({ type: "CLEAR_TAG_FORM" });
//             }

//             if(store.getState().appReducer.dateQuery) {
//                 store.dispatch({ type: "CLEAR_DATE_FORM" });
//             }
//         })
//         .catch(function(error) {
//             console.log(error);
//         });
// }

// export function* uploadArtist(data) {
//     const url = apiUrl + "aev/uploadartist";
//     yield axios
//         .post(url, data)
//         .then(function(response) {
//             const msg = {
//                 status: response.status,
//                 data: [{ code: 200, msg: response.data.message }]
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         })
//         .catch(function(error) {
//             const arr = Object.values(error.response.data.error.errors).map(
//                 err => {
//                     return { code: 500, msg: err.message };
//                 }
//             );
//             const msg = {
//                 status: error.response.status,
//                 data: arr
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         });
// }

// export function* uploadParticipant(data) {
//     const url = apiUrl + "aev/uploadparticipant";
//     yield axios
//         .post(url, data)
//         .then(function(response) {
//             const msg = {
//                 status: response.status,
//                 data: [{ code: 200, msg: response.data.message }]
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         })
//         .catch(function(error) {
//             const arr = Object.values(error.response.data.error.errors).map(
//                 err => {
//                     return { code: 500, msg: err.message };
//                 }
//             );
//             const msg = {
//                 status: error.response.status,
//                 data: arr
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         });
// }

// export function* uploadVisitor(data) {
//     const url = apiUrl + "aev/uploadvisitor";
//     yield axios
//         .post(url, data)
//         .then(function(response) {
//             const msg = {
//                 status: response.status,
//                 data: [{ code: 200, msg: response.data.message }]
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         })
//         .catch(function(error) {
//             const arr = Object.values(error.response.data.error.errors).map(
//                 err => {
//                     return { code: 500, msg: err.message };
//                 }
//             );
//             const msg = {
//                 status: error.response.status,
//                 data: arr
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         });
// }

// export function* uploadContactMessage(data) {
//     const url = apiUrl + "aev/uploadContactMessage";
//     yield axios
//         .post(url, data)
//         .then(function(response) {
//             const msg = {
//                 status: response.status,
//                 data: [{ code: 200, msg: response.data.message }]
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         })
//         .catch(function(error) {
//             const arr = Object.values(error.response.data.error.errors).map(
//                 err => {
//                     return { code: 500, msg: err.message };
//                 }
//             );
//             const msg = {
//                 status: error.response.status,
//                 data: arr
//             };
//             store.dispatch({
//                 type: "SHOW_POST_STATUS_MSG",
//                 payload: msg
//             });
//         });
// }
